ball = {}

started = false

--this is where we set attributes of the ball
function ball.load()
    ball.x = love.graphics.getWidth() / 2
    ball.y = love.graphics.getHeight() / 2
    ball.width = 20
    ball.height = 20
    ball.xvel = 0
    ball.yvel = 0
    ball.friction = 1
    ball.speed = 500
end

--this is where the physics are handled
function ball.physics(dt)
    ball.x = ball.x + ball.xvel * dt
    ball.y = ball.y + ball.yvel * dt
    ball.xvel = ball.xvel
    ball.yvel = ball.yvel

    if (ball.x <= 0 or ball.x >= love.graphics.getWidth()) and
        (ball.y - ball.height >= 0 or ball.y + ball.height >= love.graphics.getHeight()) then

        ball.x = love.graphics.getWidth() / 2
        ball.y = love.graphics.getHeight() / 2
        ball.xvel = 0
        ball.yvel = 0
        started = false
    end

    if (ball.y + ball.height >= love.graphics.getHeight()) or (ball.y - ball.height <= 0) then
        ball.yvel = -ball.yvel

        if ball.x < love.graphics.getWidth() / 2 then
            ball.xvel = ball.xvel
        elseif ball.x > love.graphics.getWidth() / 2 then
            ball.xvel = ball.xvel
        end
    end
end

function ball.bounce(player)
    print(love.timer.getFPS())

    if (player.num == 2) then
        if (ball.y >= player.y and ball.y <= player.y + player.height) and ball.x >= player.x - player.width  then
            ball.xvel = -ball.xvel
        end
    elseif (ball.y >= player.y and ball.y <= player.y + player.height) and ball.x <= player.x + player.width  then
        ball.xvel = -ball.xvel
    end
end

function ball.move(dt)
    if love.keyboard.isDown("return") then
         ball.shoot()
    end
end

function ball.shoot()
    if ball.yvel == 0 and ball.xvel == 0 and not started then
        ball.yvel = (ball.speed)
        ball.xvel = (ball.speed - 300)
        started = true
    end
end

function ball.draw()
    love.graphics.setColor(255,255,255)
    love.graphics.circle("line", ball.x, ball.y, ball.width, ball.height)
    love.graphics.circle("fill", ball.x, ball.y, ball.width, ball.height)
end

function ball.LOAD()
    ball.load()
end

function ball.UPDATE(dt)
    ball.physics(dt)
    ball.move(dt)
    --ball.shoot()
end

function ball.DRAW()
    ball.draw()
end