require "player"
require "menu"
require "ball"

function love.load()
	love.window.setMode(1200, 800, { fullscreen = false, resizable = true})
	love.window.setTitle("LovePong")
	Buttons.load()
	player1 = player.load({"w", "s"}, {20, 20}, 1)
	player2 = player.load({"up", "down"}, {love.graphics.getWidth() - 40, 20}, 2)

	ball.load()
end

function love.update(dt)
	if not IsActive() then
		player.update(player2, dt)
		player.update(player1, dt)

		ball.UPDATE(dt)

		ball.bounce(player1)
		ball.bounce(player2)
	end
end

function love.draw()
	if IsActive() then
		Buttons.draw()
	else
		player.draw(player1)
		player.draw(player2)

		ball.draw()
	end
end
