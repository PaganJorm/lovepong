player = {}

--this is where we set attributes of the player
function player.load(controls, xy, num)
    local self = {}
    self.num = num
    self.x = xy[1]
    self.y = xy[2]
    self.up = controls[1]
    self.down = controls[2]
    self.width = 20
    self.height = 100
    self.xvel = 0
    self.yvel = 0
    self.friction = 1
    self.speed = 1000

    return self
end

--this is where the player is drawn from
function player.draw(player)
    love.graphics.setColor(255,255,255)
    love.graphics.rectangle("fill", player.x, player.y, player.width, player.height)
end

--this is where the physics are handled
function player.physics(player, dt)
    player.x = player.x + player.xvel * dt
    player.y = player.y + player.yvel * dt
    player.xvel = player.xvel * (1 - math.min(dt * player.friction, 1))
    player.yvel = player.yvel * (1 - math.min(dt * player.friction, 1))
end

--this is where the movement is handled
function player.move(player, dt)
    if player.y + player.height >= love.graphics.getHeight() then
        player.yvel = 0
        player.y = love.graphics.getHeight() - player.height
    end

    if player.y <= 0 then
        player.yvel = 0
        player.y = 0
    end

    if love.keyboard.isDown(player.down) and player.yvel < player.speed then
        player.yvel = player.yvel + player.speed * dt
    end

    if love.keyboard.isDown(player.up) and player.yvel > -player.speed then
        player.yvel = player.yvel - player.speed * dt
    end
end

function player.update(self, dt)
    player.physics(self, dt)
    player.move(self, dt)
end