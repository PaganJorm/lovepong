BUTTON_HEIGHT = 64;
Active = nil

function NewButton(text, fn)
    return {
        text = text,
        fn = fn,
        now = false,
        last = false
    }
end

Buttons = {}
local font = nil

function IsActive()
    return Active
end

function Buttons.load()
    Active = true
    font = love.graphics.newFont(32)
    table.insert(Buttons, NewButton("New Game", function() Buttons.Remove() end))
    table.insert(Buttons, NewButton("Quit", function() love.event.quit(0) end))
end

function Buttons.Remove()
    Active = false
    Buttons = nil
end

function Buttons.draw()
    local ww = love.graphics.getWidth()
    local wh = love.graphics.getHeight()
    local margin = 16

    local button_width = ww * (1 / 3)

    local total_height = (BUTTON_HEIGHT + margin) * #Buttons
    local cursor_y = 0

    for i, button in ipairs(Buttons) do
        button.last = button.now

        local bx = (ww / 2) - (button_width / 2)
        local by = (wh / 2) - (total_height / 2) + cursor_y

        local color = {255, 255, 255, 1.0}
        local mx, my = love.mouse.getPosition()

        local hot = mx > bx and mx < bx + button_width and my > by and my < by + BUTTON_HEIGHT

        local mode = "line"

        if hot then
            mode = "fill"
        end

        button.now = love.mouse.isDown(1)

        if button.now and not button.last and hot then
            button.fn()
        end

        love.graphics.setColor(unpack(color))

        love.graphics.rectangle(mode, bx, by, button_width, BUTTON_HEIGHT)

        love.graphics.setColor(255, 255, 255, 1.0)

        if hot then
            color = {0, 0, 0, 1.0}
        end

        love.graphics.setColor(unpack(color))

        local textW = font:getWidth(button.text)
        local textH = font:getHeight(button.text)

        love.graphics.print(button.text, font, (ww / 2) - textW / 2, by + textH / 2)

        cursor_y = cursor_y + (BUTTON_HEIGHT + margin)
    end
end
